<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/sacha_theme/templates/paragraph/paragraph--ref-para-statistiques.html.twig */
class __TwigTemplate_48aace1f317e45514fad4278a76eabda71aa13c207e754dd4d592bb386ddfc77 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<section class=\"paragraph-statistiques\">
<ul>
    <li class=\"statistiques-degats-magique\">
        ";
        // line 4
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_degats_magique", [], "any", false, false, true, 4), 4, $this->source), "html", null, true);
        echo "
    </li>

    <li class=\"statistiques-degats-physique\">
        ";
        // line 8
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_degats_physique", [], "any", false, false, true, 8), 8, $this->source), "html", null, true);
        echo "
    </li>

    <li class=\"statistiques-point-de-mana\">
        ";
        // line 12
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_point_de_mana", [], "any", false, false, true, 12), 12, $this->source), "html", null, true);
        echo "
    </li>

    <li class=\"statistiques-point-de-vie\">
        ";
        // line 16
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_point_de_vie", [], "any", false, false, true, 16), 16, $this->source), "html", null, true);
        echo "
    </li> 

    <li class=\"statistiques-resistance-magique\">
        ";
        // line 20
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_resistance_magique", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
        echo "
    </li>
    
    <li class=\"statistiques-resistance-physique\">
        ";
        // line 24
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_resistance_physique", [], "any", false, false, true, 24), 24, $this->source), "html", null, true);
        echo "
    </li>
</ul>
</section>";
    }

    public function getTemplateName()
    {
        return "themes/custom/sacha_theme/templates/paragraph/paragraph--ref-para-statistiques.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 24,  72 => 20,  65 => 16,  58 => 12,  51 => 8,  44 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/sacha_theme/templates/paragraph/paragraph--ref-para-statistiques.html.twig", "/var/www/meuporgue/web/themes/custom/sacha_theme/templates/paragraph/paragraph--ref-para-statistiques.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 4);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
