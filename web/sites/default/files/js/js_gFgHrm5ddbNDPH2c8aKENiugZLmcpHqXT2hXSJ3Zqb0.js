(function ($, Drupal) {

  'use strict';

  /**
  * Set content fields to visible when tabs are created. After an action
  * being performed, stay on the same perspective.
  *
  * @param $parWidget
  *   Paragraphs widget.
  * @param $parTabs
  *   Paragraphs tabs.
  * @param $parContent
  *   Paragraphs content tab.
  * @param $parBehavior
  *   Paragraphs behavior tab.
  * @param $mainRegion
  *   Main paragraph region.
  */
  var setUpTab = function ($parWidget, $parTabs, $parContent, $parBehavior, $mainRegion) {
    var $tabContent = $parTabs.find('.paragraphs_content_tab');
    var $tabBehavior = $parTabs.find('.paragraphs_behavior_tab');
    if ($tabBehavior.hasClass('is-active')) {
      $parWidget.removeClass('content-active').addClass('behavior-active');
      $tabContent.removeClass('is-active');
      $tabBehavior.addClass('is-active');
    }
    else {
      // Activate content tab visually if there is no previously
      // activated tab.
      if (!($mainRegion.hasClass('content-active'))
        && !($mainRegion.hasClass('behavior-active'))) {
        $tabContent.addClass('is-active');
        $parWidget.addClass('content-active');
      }

      $parTabs.show();
      if ($parBehavior.length === 0) {
        $parTabs.hide();
      }
    }
  };

  /**
  * Switching active class between tabs.
  * @param $parTabs
  *   Paragraphs tabs.
  * @param $clickedTab
  *   Clicked tab.
  * @param $parWidget
  *   Paragraphs widget.
  */
  var switchActiveClass = function ($parTabs, $clickedTab, $parWidget) {
    var $clickedTabParent = $clickedTab.parent();

    $parTabs.find('li').removeClass('is-active');
    $clickedTabParent.addClass('is-active');

    $parWidget.removeClass('behavior-active content-active');
    if ($clickedTabParent.hasClass('paragraphs_content_tab')) {
      $parWidget.addClass('content-active');
      $parWidget.find('.paragraphs-add-wrapper').parent().removeClass('hidden');
    }
    else {
      $parWidget.addClass('behavior-active');
      $parWidget.find('.paragraphs-add-wrapper').parent().addClass('hidden');
    }
  };

  /**
   * Add class to first paragraph in the viewport.
   *
   * In order to have a persistent position when switching tabs,
   * we add a class to the first paragraph visible in the viewport.
   */
  var markFirstVisibleParagraph = function (totalTopOffset) {
    var $window = $(window);
    var bottomOfScreen = $window.scrollTop() + $window.height();
    var topOfScreen = $window.scrollTop() + totalTopOffset;
    var $firstParagraph = false;
    // @todo Make sure to skip non-Paragraph draggable field widget items here.
    var $allParagraphs = $('.node-form .draggable');

    $allParagraphs.each(function () {
      var $this = $(this);
      var topOfElement = $this.offset().top;
      var bottomOfElement = $this.offset().top + $this.height();

      // Search for paragraphs inside the viewport.
      if ((bottomOfScreen > topOfElement) && (topOfScreen < bottomOfElement)) {
        if ($firstParagraph) {
          // Find next best Paragraph in Viewport.
          if (topOfElement > topOfScreen ) {
            // Second top in screen or first nested in screen.
            $firstParagraph = $this;
            return false;
          }
          else if(topOfElement > bottomOfScreen) {
            // Choose previous element.
            return false;
          }
        }

        $firstParagraph = $this;
        if (topOfScreen < topOfElement) {
          // Choose this element as it starts in viewport.
          return false;
        }
      }
    });

    if ($firstParagraph) {
      // Remove potential previous marker.
      $('.first-paragraph').removeClass('first-paragraph');
      // Add the class to the first paragraph in the viewport.
      $firstParagraph.addClass('first-paragraph paragraph-hover');
    }

    return $firstParagraph;
  };

  /**
   * For body tag, adds tabs for selecting how the content will be displayed.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.bodyTabs = {
    attach: function (context) {
      var $topLevelParWidgets = $('.paragraphs-tabs-wrapper', context).filter(function() {
        return $(this).parents('.paragraphs-nested').length === 0;
      });

      // Initialization.
      $topLevelParWidgets.once('paragraphs-bodytabs').each(function() {
        var $parWidget = $(this);
        var $parTabs = $parWidget.find('.paragraphs-tabs');

        // Create click event.
        $parTabs.find('a').click(function(e) {
          var toolbarHeight = $('.toolbar-tray-horizontal').outerHeight() || 0;
          var adminToolbarsOffset = $('#toolbar-bar').outerHeight() + toolbarHeight;
          var totalTopOffset = adminToolbarsOffset + $('.paragraphs-tabs').outerHeight();
          var $firstParagraph;
          var currentParagraphOffset = 0;
          var $window = $(window);

          $firstParagraph = markFirstVisibleParagraph(totalTopOffset);

          // Set the proper window position for each tab.
          if ($firstParagraph) {
            // Maintain vertical offset in addition to the toolbar heights.
            currentParagraphOffset = $firstParagraph.offset().top - ($window.scrollTop() + totalTopOffset);
            // Ignore a negative offset.
            if (currentParagraphOffset < 0) {
              currentParagraphOffset = 0;
            }
          }

          e.preventDefault();
          switchActiveClass($parTabs, $(this), $parWidget);

          // We need to check to which position to scroll to, whenever we need to scroll.
          // If the first paragraph is the same, we maintain the scroll position, otherwise scroll to top of the paragraph.
          if ($firstParagraph) {
            $('html, body').scrollTop($firstParagraph.offset().top - totalTopOffset - currentParagraphOffset);

            // Reset the first paragraph class with a delay, in order for the background change to be visible.
            setTimeout(function() {
              $('.first-paragraph').removeClass('paragraph-hover');
            }, 1000);
          }

        });
      });

      if ($('.paragraphs-tabs-wrapper', context).length > 0) {
        $topLevelParWidgets.each(function() {
          var $parWidget = $(this);
          var $parTabs = $parWidget.find('.paragraphs-tabs');
          var $parContent = $parWidget.find('.paragraphs-content');
          var $parBehavior = $parWidget.find('.paragraphs-behavior');
          var $mainRegion = $parWidget.find('.layout-region-node-main');
          setUpTab($parWidget, $parTabs, $parContent, $parBehavior, $mainRegion);
        });
      }
    }
  };
})(jQuery, Drupal);

;
/**
 * @file
 * Paragraphs actions JS code for paragraphs actions button.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Process paragraph_actions elements.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches paragraphsActions behaviors.
   */
  Drupal.behaviors.paragraphsActions = {
    attach: function (context, settings) {
      var $actionsElement = $(context).find('.paragraphs-dropdown').once('paragraphs-dropdown');
      // Attach event handlers to toggle button.
      $actionsElement.each(function () {
        var $this = $(this);
        var $toggle = $this.find('.paragraphs-dropdown-toggle');

        $toggle.on('click', function (e) {
          e.preventDefault();
          $this.toggleClass('open');
        });

        $this.on('focusout', function (e) {
          setTimeout(function () {
            if ($this.has(document.activeElement).length == 0) {
              // The focus left the action button group, hide actions.
              $this.removeClass('open');
            }
          }, 1);
        });
      });
    }
  };

})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  Drupal.behaviors.fileValidateAutoAttach = {
    attach: function attach(context, settings) {
      var $context = $(context);
      var elements;

      function initFileValidation(selector) {
        $(once('fileValidate', $context.find(selector))).on('change.fileValidate', {
          extensions: elements[selector]
        }, Drupal.file.validateExtension);
      }

      if (settings.file && settings.file.elements) {
        elements = settings.file.elements;
        Object.keys(elements).forEach(initFileValidation);
      }
    },
    detach: function detach(context, settings, trigger) {
      var $context = $(context);
      var elements;

      function removeFileValidation(selector) {
        $(once.remove('fileValidate', $context.find(selector))).off('change.fileValidate', Drupal.file.validateExtension);
      }

      if (trigger === 'unload' && settings.file && settings.file.elements) {
        elements = settings.file.elements;
        Object.keys(elements).forEach(removeFileValidation);
      }
    }
  };
  Drupal.behaviors.fileAutoUpload = {
    attach: function attach(context) {
      $(once('auto-file-upload', 'input[type="file"]', context)).on('change.autoFileUpload', Drupal.file.triggerUploadButton);
    },
    detach: function detach(context, settings, trigger) {
      if (trigger === 'unload') {
        $(once.remove('auto-file-upload', 'input[type="file"]', context)).off('.autoFileUpload');
      }
    }
  };
  Drupal.behaviors.fileButtons = {
    attach: function attach(context) {
      var $context = $(context);
      $context.find('.js-form-submit').on('mousedown', Drupal.file.disableFields);
      $context.find('.js-form-managed-file .js-form-submit').on('mousedown', Drupal.file.progressBar);
    },
    detach: function detach(context, settings, trigger) {
      if (trigger === 'unload') {
        var $context = $(context);
        $context.find('.js-form-submit').off('mousedown', Drupal.file.disableFields);
        $context.find('.js-form-managed-file .js-form-submit').off('mousedown', Drupal.file.progressBar);
      }
    }
  };
  Drupal.behaviors.filePreviewLinks = {
    attach: function attach(context) {
      $(context).find('div.js-form-managed-file .file a').on('click', Drupal.file.openInNewWindow);
    },
    detach: function detach(context) {
      $(context).find('div.js-form-managed-file .file a').off('click', Drupal.file.openInNewWindow);
    }
  };
  Drupal.file = Drupal.file || {
    validateExtension: function validateExtension(event) {
      event.preventDefault();
      $('.file-upload-js-error').remove();
      var extensionPattern = event.data.extensions.replace(/,\s*/g, '|');

      if (extensionPattern.length > 1 && this.value.length > 0) {
        var acceptableMatch = new RegExp("\\.(".concat(extensionPattern, ")$"), 'gi');

        if (!acceptableMatch.test(this.value)) {
          var error = Drupal.t('The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.', {
            '%filename': this.value.replace('C:\\fakepath\\', ''),
            '%extensions': extensionPattern.replace(/\|/g, ', ')
          });
          $(this).closest('div.js-form-managed-file').prepend("<div class=\"messages messages--error file-upload-js-error\" aria-live=\"polite\">".concat(error, "</div>"));
          this.value = '';
          event.stopImmediatePropagation();
        }
      }
    },
    triggerUploadButton: function triggerUploadButton(event) {
      $(event.target).closest('.js-form-managed-file').find('.js-form-submit[data-drupal-selector$="upload-button"]').trigger('mousedown');
    },
    disableFields: function disableFields(event) {
      var $clickedButton = $(this);
      $clickedButton.trigger('formUpdated');
      var $enabledFields = [];

      if ($clickedButton.closest('div.js-form-managed-file').length > 0) {
        $enabledFields = $clickedButton.closest('div.js-form-managed-file').find('input.js-form-file');
      }

      var $fieldsToTemporarilyDisable = $('div.js-form-managed-file input.js-form-file').not($enabledFields).not(':disabled');
      $fieldsToTemporarilyDisable.prop('disabled', true);
      setTimeout(function () {
        $fieldsToTemporarilyDisable.prop('disabled', false);
      }, 1000);
    },
    progressBar: function progressBar(event) {
      var $clickedButton = $(this);
      var $progressId = $clickedButton.closest('div.js-form-managed-file').find('input.file-progress');

      if ($progressId.length) {
        var originalName = $progressId.attr('name');
        $progressId.attr('name', originalName.match(/APC_UPLOAD_PROGRESS|UPLOAD_IDENTIFIER/)[0]);
        setTimeout(function () {
          $progressId.attr('name', originalName);
        }, 1000);
      }

      setTimeout(function () {
        $clickedButton.closest('div.js-form-managed-file').find('div.ajax-progress-bar').slideDown();
      }, 500);
      $clickedButton.trigger('fileUpload');
    },
    openInNewWindow: function openInNewWindow(event) {
      event.preventDefault();
      $(this).attr('target', '_blank');
      window.open(this.href, 'filePreview', 'toolbar=0,scrollbars=1,location=1,statusbar=1,menubar=0,resizable=1,width=500,height=550');
    }
  };
})(jQuery, Drupal);;
